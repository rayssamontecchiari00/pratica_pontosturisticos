document.addEventListener("DOMContentLoaded", function () {
  const list = [];

  const input_image = document.querySelector("#image");
  const input_title = document.querySelector(".form-title");
  const input_description = document.querySelector(".form-description");
  const submit_button = document.querySelector(".form-button");
  const itens_result = document.querySelector(".render-result");

  submit_button.addEventListener("click", addItemToLIst);

  function addItemToLIst(event) {
    event.preventDefault();

    const itemTitle = input_title.value;
    const itemDescription = input_description.value;

    var file = input_image.files[0];

    if (itemTitle !== "" && itemDescription !== "") {
      var reader = new FileReader();

      reader.onloadend = function () {
        const item = {
          title: itemTitle,
          description: itemDescription,
          imagem: reader.result,
        };

        list.push(item);
        renderListItems();
      };

      reader.readAsDataURL(file);
    }
  }

  function renderListItems() {
    let itemsStructure = "";

    list.forEach(function (item) {
      itemsStructure += `
            <div class="turism-result">
                <img class="turism-result-image" alt="" src="${item.imagem}" id="image">
                <div class="turism-result-text">
                    <h1 class="turism-result-title">${item.title}</h1>
                    <p class="turism-result-description">${item.description}</p>
                </div>
            </div>`;
    });

    // list.forEach(function(item) {
    //     console.log(item.imagem.height())
    // })

    itens_result.innerHTML = itemsStructure;
  }
});
